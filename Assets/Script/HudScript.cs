﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudScript : MonoBehaviour
{

    public Text nameText;
    public Text levelText;
    public Slider hpSlider;

    public void setUpHUD(Unit unit) {
        nameText.text = unit.unitName;
        levelText.text = "Lvl " + unit.unitLevel;
        hpSlider.maxValue = unit.maxHP;
        hpSlider.value = unit.currHP;
    }

    public void setHp(int hp) {
        hpSlider.value = hp;
    }

    public void updateSlider(int hp) {
        hpSlider.maxValue = hp;
    }

    public void updateLvl(int level) {
        levelText.text = "Lvl " + level;
    }
}
