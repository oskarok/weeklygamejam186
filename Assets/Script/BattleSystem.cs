﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleState {START, PLAYERTURN,ENEMYTURN,WON,LOST}
public class BattleSystem : MonoBehaviour
{

    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public Transform playerBattleStation;
    public Transform enemyBattleStation;

    public GameObject daysLater;
    public GameObject UI;

    public GameObject gameOver;
    public Text gameOverText;
    public BattleState state;

    public Text dialogue;

    Unit playerUnit;
    Unit enemyUnit;

    public Sound sound;
    public HudScript playerHUD;
    public HudScript enemyHUD;
    private Vector2 screenBounds;

    public GameObject player;

    public GameObject enemy;
    
    // Start is called before the first frame update
    void Start()
    {
        state = BattleState.START;
        StartCoroutine(SetupBattle());
        //DontDestroyOnLoad(this.gameObject);

        
    }

    IEnumerator SetupBattle() {

        player = Instantiate(playerPrefab) as GameObject;
        player.transform.position = new Vector3(1f,1.1f,90f);
        playerUnit = player.GetComponent<Unit>();
        enemy = Instantiate(enemyPrefab) as GameObject;
        enemy.transform.position = new Vector3(2f,1.1f,90f);
        enemyUnit = enemy.GetComponent<Unit>();
        

        dialogue.text = "The main character approaches...";

        playerHUD.setUpHUD(playerUnit);
        enemyHUD.setUpHUD(enemyUnit);

        yield return new WaitForSeconds(2f);


        state = BattleState.PLAYERTURN;
        playerTurn();
    }


    void playerTurn() {
        dialogue.text = "What would you like to do?";
    }

    IEnumerator PlayerAttack() {

        int dmg = Random.Range(playerUnit.minAtk, playerUnit.maxAtk+1);
        bool isDead = enemyUnit.takeDmg(dmg);
        enemyHUD.setHp(enemyUnit.currHP);

        dialogue.text = "You hit " + enemyUnit.unitName + " for " + dmg + " damage";
        Sound.playSound("playeratk");
        yield return new WaitForSeconds(1f);

        if(isDead){
            state = BattleState.WON;
            StartCoroutine(endBattle());
        }
        else {
            state = BattleState.ENEMYTURN;
            StartCoroutine(enemyTurn());
        }

    }

    IEnumerator enemyTurn() {

        Sound.playSound("enemyatk");
        int dmg = Random.Range(enemyUnit.minAtk, enemyUnit.maxAtk+1);
        dialogue.text = enemyUnit.unitName + " hits you for " + dmg + " damage";

        //yield return new WaitForSeconds(1f);

        bool isDead = playerUnit.takeDmg(dmg);

        playerHUD.setHp(playerUnit.currHP);

        yield return new WaitForSeconds(1f);

        if(isDead) {
            state = BattleState.LOST;
            StartCoroutine(endBattle());
        }
        else {
            state = BattleState.PLAYERTURN;
            playerTurn();
        }
    }

    IEnumerator endBattle() {

        if(state == BattleState.WON) {
           
            
            dialogue.text = "You won the battle!";
            //enemy.transform.Rotate(new Vector3(4.3f,-6f,-85f));
            enemy.SetActive(false);
            Sound.playSound("enemydeath");
            yield return new WaitForSeconds(2f);
            enemyUnit.deaths += 1;
            daysLater.SetActive(true);
            UI.SetActive(false);
            // playerPrefab.SetActive(false);
            // enemyPrefab.SetActive(false);
            player.SetActive(false);
            
            yield return new WaitForSeconds(2f);
            
            
            StartCoroutine(resetGame());
            
            // SceneManager.LoadScene("Scene2");
           
            //SceneManager.LoadScene("MainScene");
            //SetupBattle();
            
        }
        else if(state == BattleState.LOST) {
            dialogue.text = "You lost the battle ";
            player.transform.Rotate(new Vector3(-7.6f,5f,-90f));
            Sound.playSound("playerdeath");
            yield return new WaitForSeconds(2f);

            gameOver.SetActive(true);
            gameOverText.text = enemyUnit.unitName + " needed " + (enemyUnit.deaths+1) + " tries to beat you.";

        }

    }

    IEnumerator resetGame() {
        UI.SetActive(true);
        daysLater.SetActive(false);
        player.SetActive(true);
        enemy.SetActive(true);
        //enemy.transform.Rotate(new Vector3(-4f,6f,-85f));
        // playerPrefab.SetActive(true);
        // enemyPrefab.SetActive(true);

        dialogue.text = "The main character approaches stronger than before...";
        

        playerUnit.currHP = playerUnit.maxHP;
        playerHUD.setHp(playerUnit.currHP);
        
        enemyUnit.maxHP = enemyUnit.maxHP + 10;
        enemyUnit.currHP = enemyUnit.maxHP;
        enemyUnit.minAtk = enemyUnit.minAtk + 2;
        enemyUnit.maxAtk = enemyUnit.maxAtk + 5;
        enemyUnit.unitLevel = enemyUnit.unitLevel + 5;
        enemyHUD.updateSlider(enemyUnit.currHP);
        enemyHUD.setHp(enemyUnit.currHP);
        enemyHUD.updateLvl(enemyUnit.unitLevel);
        
        yield return new WaitForSeconds(2f);
       

        state = BattleState.PLAYERTURN;
        playerTurn();


    }

    public void TryAgainBtn() {
        if(state != BattleState.LOST) {
            return;
        }
        gameOver.SetActive(false);
        Destroy(enemy);
        Destroy(player);
        StartCoroutine(SetupBattle());
    }
    public void AttackBtn(){
        if(state != BattleState.PLAYERTURN) {
            return;
        }

        StartCoroutine(PlayerAttack());
    }
   
    
}
