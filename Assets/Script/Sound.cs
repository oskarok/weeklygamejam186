﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{

    public static AudioClip enemyAtk, playerAtk,enemyDeath,playerDeath;

    public static AudioSource audioSrc;

   
    // Start is called before the first frame update
    void Start()
    {
        enemyAtk = Resources.Load<AudioClip> ("enemyatk");
        playerAtk = Resources.Load<AudioClip> ("playeratk");
        enemyDeath = Resources.Load<AudioClip> ("enemydeath");
        playerDeath = Resources.Load<AudioClip> ("playerdeath");
        audioSrc = GetComponent<AudioSource> ();
    }

    public static void playSound(string sound) {
        switch(sound) {
            case "enemyatk":
                audioSrc.PlayOneShot(enemyAtk);
                break;
            case "playeratk":
                audioSrc.PlayOneShot(playerAtk);
                break;
            case "enemydeath":
                audioSrc.PlayOneShot(enemyDeath);
                break;
            case "playerdeath":
                audioSrc.PlayOneShot(playerDeath);
                break;
            
        }
    }

}
