﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public string unitName;
    public int unitLevel;
    public int minAtk;
    public int maxAtk;
    public int maxHP;
    public int currHP;

    public int deaths;

    public bool takeDmg(int dmg) {
        
        currHP -= dmg;

        if(currHP <=0) {
            return true;
        }
        else {
            return false;
        }
    }

}
